<?php
    $title       = "Banquinho de Praia Dobrável";
    $description = "O banquinho de praia dobrável é um objeto que pode ser levado e usado tanto na praia, quanto em sítios e até mesmo em casa para tomar um banho de sol. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O banquinho de praia dobrável é um objeto que pode ser levado e usado tanto na praia, quanto em sítios e até mesmo em casa para tomar um banho de sol. Sendo um material extremamente fácil de ser transportado, o banquinho de praia dobrável é a melhor opção para você que não quer fazer muito esforço em seu momento de lazer e sim, ter mais praticidade. A Sunblock está atuando nesses ramo há mais de 10 anos, atualizando cada vez mais nossos produtos para que você tenha o resultado que espera ao adquiri-los. Buscamos sempre fazer com que todos os produtos sejam feitos com a mais alta qualidade desde a execução, até o momento em que eles estiverem em suas mãos para o seu conforto e lazer.</p>
<p>Assim como a cadeira, o banquinho de praia dobrável serve para que você tenha mais facilidade, praticidade e conforto quando estiver mo momento de lazer com a família ou até mesmo sozinho. Fazemos sempre os nossos produtos de forma única, para que você possa relaxar enquanto toma aquele sol ou queria somente fica numa área externa, podendo ser na praia ou algum lugar com piscina e até mesmo um ambiente aberto de sua casa. Nosso banquinho de praia dobrável é feito com materiais para deixá-lo leve para que você possa transportá-lo com qualquer veículo que possuir ou até mesmo manualmente. Fabricamos diversos artigos de praia para que você tenha tudo o que precisa para o seu momento de lazer, de uma forma prática e principalmente com a mais alta qualidade. Navegue em nosso site para conhecer todos os produtos que possuímos em nossa empresa e até mesmo para ter os melhores itens de praia. Nossos profissionais possuem anos de experiência e atuação para que possam te entregar tal produto da forma que você deseja. Portanto, não deixe de nos consultar para conhecer mais sobre a melhor fabricante de artigos de praia do país.</p>
<h2>Mais detalhes sobre banquinho de praia dobrável</h2>
<p>Não deixe para a última hora e garanta já o melhor banquinho de praia dobrável para você. Será um prazer fazer com que nossos produtos estejam num momento tão gostoso para você e sua família.</p>
<h2>A melhor opção para banquinho de praia dobrável</h2>
<p>Possuímos diversos meios de contato para que você possa falar conosco e tirar todas as suas dúvidas sobre nosso banquinho de praia dobrável,que fabricamos. Conte conosco!</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
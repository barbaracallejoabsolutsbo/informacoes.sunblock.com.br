<?php
    $title       = "Base para Guarda-sol";
    $description = "Nada como os momentos de lazer curtindo em baixo do sol, para isso, é importante se proteger, pois os raios ultravioletas do sol podem causar danos severos à nossas peles e olhos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Nada como os momentos de lazer curtindo em baixo do sol, para isso, é importante se proteger, pois os raios ultravioletas do sol podem causar danos severos à nossas peles e olhos, por tanto é importante se proteger com protetores solar e com o uso de guarda-sol, com as <strong>bases para guarda-sóis</strong> que a Sunblock fornece você vai ter um produto útil e cheio de estilo. Esta <strong>base para guarda-sol</strong> foi projetada para fixar o guarda-sol de forma mais cômoda, ele ajuda a proteger você e sua família dos raios solares. Produzido com material de al resistência e durabilidade, pois nós da Sunblock utilizamos produtos de primeira linha na confecção de nossa <strong>base para guarda-sol</strong>, para que você e sua família fiquem confortáveis ao curtir um dia ensolarado, as <strong>bases para guarda-sóis</strong> são fáceis de limpar: basta um pano macio e umedecido apenas com água para garantir a limpeza, para preservar a qualidade do produto, aconselha-se guardá-lo quando não estiver em uso. A <strong>base para guarda-sol</strong> é uma peça prática e funcional para os momentos ao sol.</p>

<h2>Escolha a Sunblock quando o assunto for <strong>bases para guarda-sóis</strong>.</h2>

<p>A <strong>base para guarda-sol</strong> que nós da Sunblock fazemos são práticas e resistentes, e darão o suporte necessário para você aproveitar belos dias ensolarados de forma prática e saudável, nossas <strong>bases para guarda-sóis</strong> foram projetada exclusivamente para manter o guarda-sol firme no local, sustentar ombrelones centrais, garantindo estabilidade e eliminando o risco de tombamentos ou acidentes.</p>

<p>A nossa produção de <strong>base para guarda-sol</strong> conta com 4 tipos de <strong>bases para guarda-sóis</strong>:</p>

<ul>

<li>Base Quadrada</li>

<li>Base Redonda</li>

<li>Base com Torre de Ferro</li>

<li>Base de PVC</li>

</ul>

<p>A definição de qual tipo de <strong>base para guarda-sol</strong> depende exclusivamente do gosto do cliente, pois as <strong>bases para guarda-sóis</strong> produzidas pela Sunblock são de extrema qualidade e contam com a fiscalização de uma equipe muito qualificada do início ao fim.</p>

<p>Atuamos no mercado de GUARDA-SOL, GUARDA-CHUVA, OMBRELLONE, CADEIRAS DE PRAIA, TENDAS, CARRINHOS QUE VIRAM MESA e <strong>base para guarda-sol</strong> promocionais, com a melhor qualidade do Brasil.</p>

<p>Nosso corpo técnico é altamente capacitado para oferecer soluções rápidas e precisas para atender suas necessidades. Colocamo-nos à inteira disposição para prestar-lhes serviços na certeza de lhe oferecermos qualidade.</p>

<p>Por tanto se estiver procurando pela melhor opção do mercado em <strong>base para guarda-sol</strong> entre em contato conosco teremos prazer em atendê-los.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
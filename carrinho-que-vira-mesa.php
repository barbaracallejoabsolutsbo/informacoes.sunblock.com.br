<?php
    $title       = "Carrinho que Vira Mesa";
    $description = "O carrinho que vira mesa da marca Sunblock é fabricado em estrutura de alumínio de excelente qualidade, capacidade média de cinco cadeiras e sua estrutura é feita em alumínio com tampo em polipropileno.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Faça de suas férias, final de semana ou verão, um lugar ainda mais divertido com carrinho que vira mesa oferecido pela melhor fabricante de artigos para praia a Sunblock. Para aproveitar o dia na praia, o carrinho que vira mesa é seu companheiro mais fiel, já que suporta até 15 kg de coisas que você precisar para passar um dia agradável com seus familiares na praia. Este carrinho vira mesa ainda vira uma mesinha e você pode fazer refeições, jogar, deixar bolsas e caixinhas de som em cima para se divertir ainda mais, a estrutura do carrinho de praia produzido pela Sunblock é em alumínio de excelente qualidade e tampo em polipropileno, produtos esses que garantem durabilidade já que seu carrinho que vira mesa não enferruja, mesmo em regiões litorâneas. Para maior segurança, acompanha corda elástica com gancho para fixar seus pertences durante o transporte.</p>
<p><br />O carrinho que vira mesa da marca Sunblock é fabricado em estrutura de alumínio de excelente qualidade, capacidade média de cinco cadeiras e sua estrutura é feita em alumínio com tampo em polipropileno, incluindo rebites e encaixes, o que evita que seu carrinho que vira mesa enferruje, mesmo em regiões litorâneas. Nosso carrinho que vira mesa também possui um apoio todo em alumínio para você colocar a sua esteira e guarda-sol e carregá-los bem presos até chegar na praia, o carrinho que vira mesa conta também com uma bolsinha porta objetos em PVC para você guardar seus pertences pessoais, como chave, óculos.</p>
<p><br /> Na praia o carrinho de praia também auxilia se transformando (vira) em uma mesa de apoio para bolsas, bebidas, copos ou o que imaginar.</p>
<h3><br /> Especificações do carrinho que vira mesa Sunblock.</h3>
<p><br /> carrinho que vira mesa:</p>
<p><br /> Diversas estampas<br /> Estrutura de alumínio<br /> Inclui rebites e encaixes, o que evita que seu carrinho enferruje, mesmo em regiões litorâneas.<br /> Possui apoio para Cadeiras, Guarda-Sol entre outros.<br /> 01 unidades<br /> 01 volumes<br /> Garantia: 03 meses contra defeito de fabricação</p>
<p><br /> Vale lembrar que nosso carrinho que vira mesa em estrutura de alumínio e rodas que não afundam na areia. Possibilita transportar cadeira, guarda-sol, esteira e toalha. Inclui também um bolso para guardar objetos pequenos no nosso carrinho que vira mesa.<br /> Transforma-se em mesa, com encaixe para copos e latinhas de bebidas.</p>
<p><br /> -Atuamos no mercado de GUARDA-SOL, GUARDA-CHUVA, OMBRELLONE, CADEIRAS DE PRAIA, TENDAS e CARRINHOS QUE VIRAM MESA promocionais, com a melhor qualidade do Brasil.</p>
<p><br /> Nosso corpo técnico é altamente capacitado para oferecer soluções rápidas e precisas para atender suas necessidades. Colocamo-nos à inteira disposição para prestar-lhes serviços na certeza de lhe oferecermos qualidade</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
<?php
    $title       = "Espreguiçadeira de Madeira";
    $description = "A espreguiçadeira de madeira é uma ótima opção para você que busca melhorar o visual ao redor de sua piscina, ou para ficar lendo um livro e até mesmo tirar aquele cochilo enquanto toma um bom banho de sol. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A espreguiçadeira de madeira é uma ótima opção para você que busca melhorar o visual ao redor de sua piscina, ou para ficar lendo um livro e até mesmo tirar aquele cochilo enquanto toma um bom banho de sol. Podendo ser levada até mesmo à praia, a nossa espreguiçadeira de madeira é feita com os melhores materiais para que tenham longa duração junto com o conforto que cada um de nossos clientes merecem. Estamos há mais de 10 anos fabricando diversos artigos de praia aprimorando cada vez mais a qualidade de nossos produtos, para que ao obter qualquer um deles, possamos atender e até mesmo superar as suas expectativas.</p>
<p>Garantimos que você  não encontrará uma espreguiçadeira de madeira melhor que a da Sunblock pois em todos os detalhes do processo da execução, fazendo de forma única e exclusiva para que você tenha o conforto que deseja no lugar em que estiver. Ao adquirir a nossa espreguiçadeira de madeira, além de deixar o ambiente mais elegante, você não terá de se preocupar em danificar a sua coluna, pois uma das nossas maiores prioridades é fazer com que nossos clientes tenham experiências incríveis ao adquirirem qualquer um de nossos produtos. Além desse produto, possuímos todos os artigos de praia possíveis para que você tenha todos os equipamentos necessário para o dia de sol perfeito. Consulte mais em nosso site para conhecer nossos demais produtos para o seu momento de diversão em lazer, com a família, amigos ou até mesmo naquele momento sozinho para distração da mente. Temos profissionais extremamente qualificados que fazem todos os nossos produtos da forma mais cautelosa, para que ao receber nossa espreguiçadeira de madeira ou demais artigos de praia que possuímos,  todas as necessidades que você tenha ao buscar nossa empresa, sejam correspondidas. Contudo, não deixe de falar com os nossos profissionais para que possamos tirar todas as suas dúvidas sobre os produtos que possuímos e para que você os obtenha com segurança e de forma rápida.</p>
<h2>Mais detalhes sobre espreguiçadeira de madeira</h2>
<p>Garanta com antecedência a nossa espreguiçadeira de madeira para que no momento em que você desejar tomar aquele sol, você tenha o máximo de conforto e lazer possível. Aguardamos ansiosamente pelo seu contato!</p>
<h2>A melhor opção para espreguiçadeira de madeira.</h2>
<p>Todos os nossos meios de contratos estão disponíveis para que você possa falar colosso e tirar todas as suas dúvidas antes de garantir nossos produtos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
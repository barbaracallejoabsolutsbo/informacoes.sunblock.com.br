<?php
    $title       = "Fabricante de Artigos de Praia";
    $description = "ossos valores consistem em manter nosso padrão de qualidade, estabelecendo uma relação de confiança com os nossos clientes, que procuram sempre a melhor fabricante de artigos de praia.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Sunblock, é fabricante de artigos de praia há muitos anos, somos fabricante de artigos de praia como por exemplo - cadeiras de praia, ombrellones, guarda-sóis, carrinhos, bases, etc. Para ser excelência em fabricante de artigos de praia contamos com produtos de alta qualidade, garantindo durabilidade, conforto, resistência e preço. Optamos por manter a característica principal dos nossos produtos, a qualidade que sempre foi nosso diferencial como fabricante de artigos de praia e o que nos trouxe a fidelização dos clientes que já puderam comprovar a durabilidade dos produtos oferecidos.</p>
<p><br /> Nossa política de fabricante de artigos de praia é proporcionar sempre a melhor experiência aos nossos clientes, por isso, nossos produtos são montados por especialistas, um a um, e conferidos detalhadamente para levar o nosso nome e qualidade da JVL fabricante de artigos de praia.</p>
<p><br /> Nossos valores consistem em manter nosso padrão de qualidade, estabelecendo uma relação de confiança com os nossos clientes, que procuram sempre a melhor fabricante de artigos de praia, não abrindo mão de utilizar apenas matérias-primas que nos leve a entregar um produto de excelência, e nos deixem em vantagem na corrida de agradar nossos clientes.</p>
<p><br /> Escolha a Sunblock como sua fabricante de artigos de praia.</p>
<p><br /> Para se referência em fabricante de artigos de praia nós criamos um novo conceito em cadeiras de praia, e todos os outros artigos necessários para se passar um ótimo verão ou férias, com todos os componentes anti ferrugem, reforço duplo e triplo em alumínio, rebites maciços em alumínio e parafusos em inox, garantimos o melhor conforto e durabilidade que uma fabricante de artigos de praia.</p>
<p><br /> O verão impulsiona os negócios sazonais e a venda de produtos típicos da estação como as esteiras, cadeiras de praia e guarda-sóis, alo que ajuda muito e empresa fabricante de artigos de praia.</p>
<p><br /> Cansado de ter de carregar as cadeiras e esteiras para a família, nós fabricante de artigos de praia colocamos a sua disposição diferentes produtos, leves e práticos: como carrinhos que viram mesa ou carrinhos com bolsas e zíper para guardar os volumes e miudezas.</p>
<p><br /> A fabricante de artigos de praia pensa em todos os meios possíveis para tornar o momento de lazer da sua família e de seus amigos o mais agradável possível, então não pense duas vezes e lembre-se da Sunblock quando for procurar pela melhor fabricante de artigos de praia.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
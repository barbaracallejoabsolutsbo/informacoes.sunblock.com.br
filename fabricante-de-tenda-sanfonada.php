<?php
    $title       = "Fabricante de Tenda Sanfonada";
    $description = "Por sermos fabricante de tenda sanfonada, possuímos diversos tipos de tenda. Ou seja, independente da forma ou cor que deseja a sua tenda, garantimos que você encontrará na Sunblock.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A tenda sanfonada pode ser utilizada para diversos meios, como por exemplo churrasco em família ou com os amigos. E para você que é empreendedor e deseja levar seu negócio  para rua. Podendo também ser levada à praia num dia de muito sol. E para que em qualquer um desses eventos não ocorra frustrações é de extrema importância buscar por uma fabricante de tenda sanfonada. Estamos atuando nesse ramo há mais de 10 anos fornecendo sempre nossos produtos com extrema qualidade para aqueles que nos procuram. Durante todo o processo de fabricação, nossos profissionais acompanham com extrema atenção e cautela para que você não receba com nenhuma avaria. Portanto, garantimos que não há lugar melhor que a Sunblock, fabricante de tenda sanfonada, para voce adquirir esse e demais outros artigos de praia.</p>
<p>Como fabricante de tenda sanfonada, possuímos tendas da cor e do formato que deseja. Buscamos sempre atender as necessidades de nossos clientes e com isso fazemos sempre a questão de mantermos contato com cada um deles, até que nossos produtos sejam entregues. É importante que você escolha a fabricante de tenda sanfonada certa, para que você não tenha nenhum prejuízo em seu momento de lazer. E garantimos a você que usamos dos mais renomados materiais para fabricar nossos produtos, para que você possa utilizá-los durante anos, mantendo sempre a qualidade e conforto que deseja. Além de sermos fabricante de tenda sanfonada, produzimos diversos outros produtos para que você tenha do o conjunto do que é necessário para o seu dia de praia, ou um bom churrasco na área externa de sua casa.</p>
<p>Nossos longos anos de experiência, juntamente com todos os nossos profissionais, fizeram com que hoje nos tornássemos a melhor fabricante de artigos de praia do país. Com a Sunblock, suas expectativas sobre nossos produtos não será somente alcançada, como superada.</p>
<h2>Mais detalhes sobre Fabricante de tenda sanfonada</h2>
<p>Por sermos fabricante de tenda sanfonada, possuímos diversos tipos de tenda. Ou seja, independente da forma ou cor que deseja a sua tenda, garantimos que você encontrará na Sunblock. Pois além de produzirmos de forma padrão, personalizamos nossos produtos para que fique cada vez mais de seu gosto.</p>
<h2>A melhor opção para fabricante de tenda sanfonada</h2>
<p>Queremos sempre fazer com que nossos clientes se sintam satisfeitos ao garantir nossos produtos. Por isso, nossos meios de contatos são de fácil acesso para mantermos o contato sempre que for necessário!</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
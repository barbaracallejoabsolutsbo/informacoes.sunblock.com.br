<?php
    $title       = "Guarda-chuva Portaria";
    $description = "Garanta já conosco seu guarda-chuva portaria para que você tenha nossos produtos da forma que deseja. Será um prazer atendermos todas as suas necessidades através de nossos serviços.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Um guarda-chuva portaria é uma ótima opção para você que possui uma empresa e precisa manter a identificação de seus funcionários, até mesmo para ajudá-los em dias muito ensolarados ou muito chuvosos. Os guarda-chuva portaria necessitam que sejam feitos com materiais de extrema qualidade, por seu contínuo uso. E a Sunblock faz a questão de sempre se atualizar a modernidade e tecnologia dos materiais para a fabricação, para que os nossos produtos durem longos anos em suas mãos, mantendo sempre a qualidade que prometemos. Estamos há mais de 10 anos atuando nesse mercado, fabricando artigos de praia e relacionados, nos aprimorando cada vez mais as necessidades de nossos clientes para que obtenham nossos produtos da forma que assim desejam. Visamos sempre priorizar as vontades de nossos clientes e por volta disso, sempre fabricamos nossos produtos da forma e quantidade que deseja.</p>
<p>Podendo ser personalizado ou não, o guarda-chuva portaria não é difícil de ser transportado, como muitos pensam por conta de seu tamanho. Utilizamos sempre os melhores materiais em nossa fabricação para que você tenha a praticidade que espera perante aos nossos produtos. Além de servir para quem possui determinada empresa, o guarda-chuva portaria serve para você que prefere um guarda chuva maior para se proteger de fortes chuvas. E por dar para utilizá-lo para diversas ocasiões, qualquer pessoa que necessita de nosso guarda-chuva portaria conseguirá adquiri-lo com a Sunblock. Garantimos que você não encontrará qualidade melhor para seu guardada chuva portaria, do que com a Sunblock. Pois, o conjunto de nossos profissionais analisam cada processo de fabricação com extrema atenção e cautela para que você tenha o conforto, segurança e praticidade que tanto deseja. Fabricamos diversos outros tipos de guarda chuva e até mesmo artigos de praia, em nossos site há todos os modelos dos produtos que produzimos, sejam eles para praia ou não.</p>
<h2>Mais detalhes sobre guarda-chuva portaria</h2>
<p>Garanta já conosco seu guarda-chuva portaria para que você tenha nossos produtos da forma que deseja. Será um prazer atendermos todas as suas necessidades através de nossos serviços. Estamos sempre disponível para quando quiser entrar em contato conosco!</p>
<h2>A melhor opção para guarda-chuva portaria</h2>
<p>Nossos profissionais que são altamente qualificados estão sempre preparados e disponíveis para te atender. Nossos meios de contatos são de fácil acesso para que você consiga tirar dúvidas de onde estiver, no momento que desejar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
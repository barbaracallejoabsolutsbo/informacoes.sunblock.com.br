<?php
    $title       = "Guarda-sol com Base";
    $description = "Se você busca aprimorar o ambiente externo de sua casa, ou até mesmo uma viagem em família, buscar por um guarda-sol com base é sua melhor opção.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você busca aprimorar o ambiente externo de sua casa, ou até mesmo uma viagem em família, buscar por um guarda-sol com base é sua melhor opção. Pois você garantirá segurança e conforto somente com um único objeto. Garantir o guarda-sol com base não é somente importante para o seu conforto mas para que você não tenha nenhum problema em duas ensolarados ou até mesmo chuvosos. Estamos há mais de 10 anos fabricando diversos artigos de praia com extrema qualidade, aprimorando sempre novas técnicas, nos adaptando as novas tecnologias dos melhores materiais, para que nossos clientes possam usar nossos produtos por longos anos. Nossos profissionais foram altamente qualificados para executarem e acompanharem cada produto do início ao fim. Portanto, não há com o que se preocupar ao constatar nossa fábrica de guarda-sol com base.</p>
<p>Se você é até mesmo um distribuidor, garantir nosso guarda-sol com base em primeira mão, da forma e quantidade que deseja , a Sunblock é a melhor empresa para você. Um de nossos diferenciais é que fazemos questão de nos atualizarmos sempre a nova tecnologia para que nossos clientes tenham a praticidade que desejam. O nosso guarda sol além de ajudar nesse quesito de praticidade, garante também a sua segurança. Por sermos fabricante, possuímos diversos modelos para que você obtenha o que mais se adeque a você. Os valores de nossos produtos são acessíveis, para que você possa garantir sei guarda-sol com base no momento em que deseja, conforme a sua necessidade. Portanto, independente de como for usá-lo e na quantidade que quiser, a Sunblock atenderá seus objetivos e expectativas perante aos nossos produtos. Além de fabricamos variados tipos de guarda sol, produzimos diversos outros artigos de praia para que você garanta com antecedência tudo o que precisa para o seu momento de lazer.</p>
<h2>Mais detalhes sobre guarda-sol com base</h2>
<p>Não hesite em nos procurar para garantir o seu guarda-sol com base. Na Sunblock, fabricamos os produtos com os melhores materiais para que você possa recebê-los com a melhor qualidade. Afirmamos que não há lugar melhor para se recorrer quando pensar em obter artigos de praia.</p>
<h2>A melhor opção para guarda-sol com base</h2>
<p>Estamos sempre disponíveis para te atender, através de nossos meios de contratos para que você possa tirar todas as dúvidas que possuir sobre esse ou demais produtos que fabricamos na Sumblock. Conte sempre conosco!</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
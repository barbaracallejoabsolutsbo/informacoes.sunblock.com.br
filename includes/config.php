<?php

    // Principais Dados do Cliente
    $nome_empresa = "Sunblock";
    $emailContato = "contato@sunblock.com.br";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => "Sunblock",
            "rua" => "Rua Cabo João Fagundes Machado 26",
            "bairro" => "Parque Novo Mundo",
            "cidade" => "São Paulo",
            "estado" => "São Paulo",
            "uf" => "SP",
            "cep" => "02144-050",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "11",
            "telefone" => "4212-5974",
            "whatsapp" => "94729-5685",
            "link_maps" => "" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.sunblock.com.br/",
        // URL online
        "https://www.informacoes.sunblock.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Banquinho de Praia Dobrável",
"Espreguiçadeira de Madeira",
"Fabricante de Esteira de Praia",
"Fabricante de Banquinho Dobrável",
"Fabricante de Base para Guarda-sol",
"Fabricante de Base para Ombrelone",
"Fabricante de Cadeira de Praia de Alumínio",
"Fabricante de Cadeira de Praia de Sentar",
"Fabricante de Cadeira de Praia de Deitar",
"Fabricante de Cadeira de Praia Personalizada",
"Fabricante de Carrinho de Praia",
"Fabricante de Espreguiçadeira de Madeira",
"Fabricante de Esteira",
"Fabricante de Guarda-chuva Personalizado",
"Fabricante de Guarda-sol de Alumínio",
"Fabricante de Guarda-sol para Praia",
"Fabricante de Guarda-sol Personalizado",
"Fabricante de Ombrelone de Madeira Personalizado",
"Fabricante de Ombrelone Personalizado",
"Fabricante de Tenda Sanfonada",
"Fabricante de Tenda Sanfonada Personalizada",
"Guarda-chuva Portaria",
"Guarda-chuva Recepção",
"Guarda-sol com Base",
"Tenda Sanfonada Personalizada",
"Base para Guarda-sol",
"Base para Ombrelone",
"Cadeira de Praia de Alumínio",
"Cadeira de Praia Preço",
"Cadeira de Praia",
"Cadeira de Praia de Sentar",
"Cadeira de Praia de Deitar",
"Cadeira Personalizada",
"Cadeira Personalizada",
"Carrinho de Praia",
"Carrinho que Vira Mesa",
"Fabricante de Artigos de Praia",
"Fabricante de Cadeira de Praia  ",
"Fabricante de Carrinho que Vira Mesa",
"Fabricante de Gazebo para Praia",
"Fabricante de Guarda-chuva",
"Fabricante de Guarda-sol de Madeira",
"Fabricante de Guarda-sol Grande",
"Fabricante de Guarda-sol para Piscina",
"Fabricante de Guarda-sol",
"Fabricante de Ombrelone de Madeira",
"Fabricante de Tenda para Praia",
"Gazebo para Praia Preço",
"Gazebo para Praia   ",
"Guarda-chuva Personalizado",
"Guarda-sol Alumínio",
"Guarda-sol Madeira",
"Guarda-sol de Praia Grande",
"Guarda-so de Praia Preço",
"Guarda-sol Grande",
"Guarda-sol para Jardim",
"Guarda-sol para Piscina",
"Guarda-sol Personalizado",
"Ombrelone com Base",
"Ombrelone de Madeira",
"Omrelone Grande",
"Ombrelone Personalizado",
"Ombrelone Preço",
"Tenda Personalizada",
"Tenda Sanfonada"
    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */
<?php
    $title       = "Ombrelone Grande";
    $description = "O ombrelone grande que nós da Sunblock fazemos são práticas e resistentes, e darão o suporte necessário para você aproveitar belos dias ensolarados de forma prática e saudável.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Nada como os momentos de lazer curtindo em baixo do sol, para isso, é importante se proteger, pois os raios ultravioletas do sol podem causar danos severos à nossas peles e olhos, por tanto é importante se proteger com protetores solar e com o uso de guarda sol, com o ombrelone grande que a Sunblock fornece você vai ter um produto útil e cheio de estilo. Este ombrelone grande foi projetado com os melhores materiais para que ser o mais cômodo possível, ele ajuda a proteger você e sua família dos raios solares. Produzido com material de alta resistência e durabilidade, pois nós da Sunblock utilizamos produtos de primeira linha na confecção de nosso ombrelone de grande, para que você e sua família fiquem confortáveis ao curtir um dia ensolarado, o ombrelone grande é fácil de limpar: basta um pano macio e umedecido apenas com água para garantir a limpeza, para preservar a qualidade do produto, aconselha-se guardá-lo quando não estiver em uso. O ombrelone grande é uma peça prática e funcional para os momentos ao sol.</p>
<h2><br /> Escolha a Sunblock quando o assunto for ombrelone grande.</h2>
<p><br /> O ombrelone grande que nós da Sunblock fazemos são práticas e resistentes, e darão o suporte necessário para você aproveitar belos dias ensolarados de forma prática e saudável, nosso ombrelone grande foi projetada exclusivamente para manter o guarda-sol firme no local, sustentar ombrelones centrais, garantindo estabilidade e eliminando o risco de tombamentos ou acidentes.</p>
<h3><br /> A nossa produção de ombrelone conta com 4 tipos de ombrelone grande:</h3>
<p><br /> <br /> Base Quadrada<br /> Base Redonda<br /> Base com Torre de Ferro<br /> Base de PVC<br /> <br /> A definição de qual tipo de ombrelone grande depende exclusivamente do gosto do cliente, pois o ombrelone grande produzido pela Sunblock é de extrema qualidade e conta com a fiscalização de uma equipe muito qualificada do início ao fim.<br /> Atuamos no mercado de GUARDA-SOL, GUARDA-CHUVA, OMBRELLONE, CADEIRAS DE PRAIA, TENDAS, CARRINHOS QUE VIRAM MESA e ombrelone grande promocionais, com a melhor qualidade do Brasil.<br /> Nosso corpo técnico é altamente capacitado para oferecer soluções rápidas e precisas para atender suas necessidades. Colocamo-nos à inteira disposição para prestar-lhes serviços na certeza de lhe oferecermos qualidade.<br /> Por tanto se estiver procurando pela melhor opção do mercado em ombrelone grande entre em contato conosco teremos prazer em atendê-los.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>
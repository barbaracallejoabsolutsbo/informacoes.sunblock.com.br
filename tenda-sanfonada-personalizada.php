<?php
    $title       = "Tenda Sanfonada Personalizada";
    $description = "Ao garantir sua tenda sanfonada personalizada, com a melhor fabricante de artigos de praia do país, você verá que não precisará mais se preocupar em ter de procurar  por tendas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A tenda sanfonada personalizada serve para você que é empreendedor e deseja aumentar a visibilidade de seu negócio, ou até mesmo a você que queria fazer algo especial com sua família. Ao consultar a Sunblock para fazer sua tenda sanfonada personalizada, você verá que haverá diversos meios em que você possa utilizá-la. Nosso maior objetivo atendermos todas as necessidades de cada um de nossos clientes, para que os mesmos garantam a praticidade, conforto e segurança que esperam. Ao longo de nossos mais de 10 anos de experiência e atuação, atendemos milhares de clientes e com cada um deles absorvermos mais conhecimento para que hoje nos tornássemos a melhor empresa nesse ramo. Contudo, garantimos que a Sunblock é o lugar certo para você recorrer quando pensar em tendas e demais outros artigos de praia.</p>
<p>Ao consultar nossas variadas tendas sanfonadas você a terá da forma e com a custumização que deseja. Seja para uma viagem, à sua empresa ou até mesmo para um churrasco, a nossa tenda sanfonada personalizada atenderá todas as suas expectativas para o momento que deseja. Nossos profissionais se especializaram não somente na execução, como também no acompanhamento na fabricação de nossos produtos para que você os receba sem nenhum tipo de avaria. A cada ano, estudamos e nos adaptamos as novas tecnologias dos melhores materiais, para que nossa tenda sanfonada personalizada possa durar anos para a sua utilização, mantendo sempre a segurança e praticidade a você. A Sunblock, além de fabricar tenda sanfonada personalizada, fabrica diversos outros artigos de praia para que você possa utilizá-los da forma que desejar. Ao garantir o conjunto de nossos produtos, você não precisará se preocupar com nada, em seu momento de lazer, pois fabricamos nossos produtos pensando sempre em todo o contexto da utilização dos mesmos.</p>
<h2>Mais detalhes sobre Tenda sanfonada personalizada</h2>
<p>Ao garantir sua tenda sanfonada personalizada, com a melhor fabricante de artigos de praia do país, você verá que não precisará mais se preocupar em ter de procurar diversas formas de se proteger do sol ou até mesmo sofisticar determinado ambiente. É um prazer para a Sunblcok poder te ajudar com os nossos produtos!</p>
<h2>A melhor opção para tenda sanfonada personalizada</h2>
<p>Fazemos a questão de sempre atender nossos clientes quando os mesmos nos procuram, portanto nossos meios de contatos são extremamente acessíveis para que você possa tirar suas devidas dúvidas de onde estiver, no momento que desejar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>